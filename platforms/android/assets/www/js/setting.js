/**
 * Created by mrkai303 on 01/12/16.
 */
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var deviceID="";
        var storage = window.localStorage;
        var myApp = new Framework7();
        var $$ = Dom7;
        var mainView = myApp.addView('.view-main', {domCache: true});
        //init logout
        var userdata=JSON.parse(storage.getItem("data"));
        console.log(userdata);
        $$('#username').val(userdata[0].Username);
        $$('#fullname').val(userdata[0].Fullname);
        $$('#address').val(userdata[0].Address);
        $$('#btnCancel').on('click',function () {
            location.href="main.html";
        });
        $$('#btnSave').on('click',function () {
            var npassword=$$('#npassword').val();
            var opassword=$$('#opassword').val();
            var cpassword=$$('#cpassword').val();
            if(npassword!=""){
                if(opassword==""){
                    if(npassword!=cpassword){
                        myApp.alert("Password Not Match",appName);
                    }else{
                        saveProcess();
                    }
                }else{
                    myApp.alert("Old Password Required",appName);
                }
            }else{
                saveProcess();
            }
        });
        function saveProcess() {
            window.plugins.spinnerDialog.show(null, "Loading ...", true);
            $$.post(configURL, {
                token: storage.getItem("token"),
                username: $$('#username').val(),
                fullname: $$('#fullname').val(),
                address: $$('#address').val(),
                opassword: md5($$('#opassword').val()),
                npassword: md5($$('#npassword').val()),
                cpassword: md5($$('#cpassword').val())
            }, function (response) {
                window.plugins.spinnerDialog.hide();
                var response = JSON.parse(response);
                if (response.status) {
                    userdata[0].Username= $$('#username').val();
                    userdata[0].Fullname= $$('#fullname').val();
                    userdata[0].Address= $$('#address').val();
                    location.href='main.html';
                } else {
                    myApp.alert(response.message,appName);
                }
            }, function (error) {
                alert("Network Error");
                window.plugins.spinnerDialog.hide();
            });
        }
    }
};
app.initialize();