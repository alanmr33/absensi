/**
 * Created by mrkai303 on 01/12/16.
 */
var app = {
    initialize: function() {
        var distance=0;
        var description="";
        var locationCurrent="0,0";
        var image="";
        var action="Check In";
        var storage = window.localStorage;
        var myApp = new Framework7();
        var $$ = Dom7;
        var mainView = myApp.addView('.view-main', {domCache: true});
        var is_checkin=storage.getItem("is_checkin");
        var checkInLat=storage.getItem("checkInLat");
        var checkInLon=storage.getItem("checkInLon");
        var isLogin = storage.getItem("login");
        if(is_checkin=="true"){
            action = "Check Out";
            $$("#action").html(action);
            $$("#image-preview").show();
            $$("#logo-main").hide();
            $$("#image-preview").css('background-image',"url('data:image/jpeg;base64,"+storage.getItem("image")+"')");
            $$("#image-preview").css('display',"block");
        }
        if(isLogin!='true') {
            location.href='login.html';
        }
        //init logout
        var userdata=JSON.parse(storage.getItem("data"));
        $$('#username').html("("+userdata[0].Username+")");
        $$('#logout').on('click',function () {
            storage.setItem("token","");
            storage.setItem("data","");
            storage.setItem("login","false");
            location.href="index.html";
        });
        $$('#setting').on('click',function () {
            location.href="settings.html";
        });
        $$('#check-in').on('click',function () {
            if(action=="Check In") {
                /*
                get description
                 */
                myApp.prompt('Please, enter location information !',appName, function (value) {
                     if(value.length<10){
                         myApp.alert("Description is too sort.",appName);
                     }else if(value.length > 100){
                         myApp.alert("Description is too long.",appName);
                     }else{
                         description=value;
                         navigator.camera.getPicture(function (imageData) {
                             window.plugins.spinnerDialog.show(null, "Loading ...", true);
                             $$.post(logURL, {
                                 token: storage.getItem("token"),
                                 latlong: locationCurrent,
                                 type: action,
                                 image : imageData,
                                 description : description
                             }, function (response) {
                                 window.plugins.spinnerDialog.hide();
                                 var response = JSON.parse(response);
                                 if (response.status) {
                                     var checkInPos=locationCurrent.split(",");
                                     checkInLat=checkInPos[0];
                                     checkInLon=checkInPos[1];
                                     storage.setItem("checkInLat",checkInLat);
                                     storage.setItem("checkInLon",checkInLon);

                                     action = "Check Out";
                                     image=imageData;
                                     $$("#action").html(action);
                                     $$("#image-preview").show();
                                     $$("#logo-main").hide();
                                     $$("#image-preview").css('background-image',"url('data:image/jpeg;base64,"+imageData+"')");
                                     $$("#image-preview").css('display',"block");
                                     storage.setItem("is_checkin","true");
                                     storage.setItem("image",imageData);
                                 } else {
                                     myApp.alert(response.message,appName);
                                 }
                             }, function (error) {
                                 myApp.alert("network error",appName);
                                 window.plugins.spinnerDialog.hide();
                             });
                         }, function (message) {
                             myApp.alert('Failed because: ' + message, appName);
                         }, {
                             targetHeight: 400,
                             targetWidth : 400,
                             quality: 50,
                             destinationType: Camera.DestinationType.DATA_URL
                         });
                     }
                });
            }else{
                checkOut();
            }
        });
        function getLocation() {
            navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });
        }
        var onSuccess = function(position) {
            distance=showDistanceFromCheckIn(checkInLat,checkInLon,position.latitude,position.longitude);
            if(is_checkin){
                if(distance>1500){
                    /*
                    show notification
                     */
                    showCheckOutNotif();
                }
                if(distance>3000){
                    checkOut();
                }
            }
            locationCurrent=position.coords.latitude+","+position.coords.longitude;
            $$('#location-checkin').html(locationCurrent);
            $$('#location').show();
        };
        var onError=function (error) {
            myApp.alert("Failed to get Location.\nPlease turn on location permission and gps !",appName);
        }
        document.addEventListener('resume',function () {
            getLocation();
        },false);
        document.addEventListener('deviceready',function () {
            //location
            if (window.cordova) {
                cordova.plugins.diagnostic.isLocationAvailable(function (available) {
                    if (available==true) {
                        getLocation();
                    } else {
                        myApp.alert("Turn On GPS And Restart App !", appName,function () {
                            cordova.plugins.diagnostic.switchToLocationSettings();
                        });
                    }
                }, function (error) {
                    console.error("The following error occurred: " + error);
                });
            }
            // window.codePush.sync(null, { updateDialog: true, installMode: InstallMode.IMMEDIATE });
        },false);
        function showCheckOutNotif() {
            // cordova.plugins.notification.local.schedule({
            //     title: "Absensi IGLO",
            //     message: "Not check out yet. Too far from office !",
            //     sound: "file://sound/notif.mp3",
            //     icon: "file://img/logo.png"
            // });
        }
        function checkOut() {
            window.plugins.spinnerDialog.show(null, "Loading ...", true);
            $$.post(logURL, {
                token: storage.getItem("token"),
                latlong: locationCurrent,
                type: action,
                description : ""
            }, function (response) {
                window.plugins.spinnerDialog.hide();
                var response = JSON.parse(response);
                if (response.status) {
                    storage.setItem("is_checkin","false");
                    $$("#logo-main").show();
                    $$("#image-preview").hide();
                    action = "Check In";
                    $$("#action").html(action);
                } else {
                    myApp.alert(response.message,appName);
                }
            }, function (error) {
                myApp.alert("network error",appName);
                window.plugins.spinnerDialog.hide();
            });
        }
    }
};
app.initialize();