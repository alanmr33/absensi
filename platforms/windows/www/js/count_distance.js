﻿/**
 * Created by Indocyber on 11/08/2017.
 */
function showDistanceFromCheckIn(latitudeBefore,longitudeBefore,latitudeAfter, longitudeAfter) {
    var R = 6378137; // Radius of the earth in m
    var dLat = deg2rad(latitudeAfter-latitudeBefore);  // deg2rad below
    var dLon = deg2rad(longitudeAfter-longitudeBefore);
    var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(latitudeAfter)) * Math.cos(deg2rad(latitudeBefore)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in m
    console.log(d);
    return d.toFixed(1);
}
function deg2rad(deg) {
    return deg * (Math.PI/180)
}