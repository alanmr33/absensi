﻿/**
 * Created by mrkai303 on 01/12/16.
 */
var app = {
    initialize: function() {
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        if(isLogin=='true') {
            location.href='main.html';
        }else{
            var deviceID="";
            var storage = window.localStorage;
            var myApp = new Framework7();
            var $$ = Dom7;
            var mainView = myApp.addView('.view-main', {domCache: true});
            var isLogin = storage.getItem("login");
            if(isLogin=='true') {
                location.href='main.html';
            }
            $$('#container-login').show();
            $$('#btnLogin').on('click', function (e) {
                window.plugins.spinnerDialog.show(null, "Loading ...", true);
                $$.post(loginURL, {
                    username: $$('#username').val(),
                    password: md5($$('#password').val()),
                    device_id : deviceID
                }, function (response) {
                    window.plugins.spinnerDialog.hide();
                    var response = JSON.parse(response);
                    if (response.status) {
                        storage.setItem("token",response.token);
                        storage.setItem("data",JSON.stringify(response.data));
                        storage.setItem("login","true");
                        location.href='main.html';
                    } else {
                        myApp.alert(response.message,appName);
                    }
                }, function (error) {
                    alert("Network Error");
                    window.plugins.spinnerDialog.hide();
                });
            });
            function successCallbackSim(result) {
                console.log(result);
                deviceID=result.subscriberId;
                $$('#serial-device').html(result.subscriberId);
            }

            function successCallback(result) {
                console.log(result);
            }
            function errorCallback(error) {
                console.log(error);
                requestSim();
            }

            function hasReadPermission() {
                window.plugins.sim.hasReadPermission(successCallback, errorCallback);
            }
            function requestReadPermission() {
                window.plugins.sim.requestReadPermission(successCallback, errorCallback);
            }
            document.addEventListener('deviceready',function () {
                cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
                    for (var permission in statuses){
                        switch(statuses[permission]){
                            case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                                console.log("Permission granted to use "+permission);
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                                console.log("Permission to use "+permission+" has not been requested yet");
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED:
                                console.log("Permission denied to use "+permission+" - ask again?");
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                                console.log("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
                                break;
                        }
                    }
                }, function(error){
                    console.error("The following error occurred: "+error);
                },[
                    cordova.plugins.diagnostic.permission.READ_PHONE_STATE,
                    cordova.plugins.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
                    cordova.plugins.diagnostic.permission.ACCESS_FINE_LOCATION,
                    cordova.plugins.diagnostic.permission.ACCESS_COARSE_LOCATION
                ]);
               requestSim();
               // requestNotif();
               // window.codePush.sync(null, { updateDialog: true, installMode: InstallMode.IMMEDIATE });
            },false);
            function requestSim() {
                if(hasReadPermission()){
                    window.plugins.sim.getSimInfo(successCallbackSim, errorCallback);
                }else{
                    requestReadPermission();
                    window.plugins.sim.getSimInfo(successCallbackSim, errorCallback);
                }
            }
            // function requestNotif() {
            //     if(!hasPermissionNotif()){
            //         cordova.plugins.notification.local.registerPermission(function (granted) {
            //             if(!granted){
            //                 myApp.alert("Notification permission is not granted.",appName);
            //             }
            //         });
            //     }
            // }
            // function hasPermissionNotif() {
            //     cordova.plugins.notification.local.hasPermission(function (granted) {
            //         return granted;
            //     });
            // }
        }
    }
};
app.initialize();